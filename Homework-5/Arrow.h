#pragma once
#include "Polygon.h"

class Arrow : public Shape
{
public:
	/**
	 * @brief creates new Arrow object`
	 * @param a Point object representing arrow "tale"
	 * @param b Point object representing arrow "head"
	 * @param type - arrow type
	 * @param name - arrow name
	 * @param color - given color
	*/
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name, Color color);
	~Arrow() override;

	/**
	 * @brief draws this arrow on a given Canvas object
	 * @param canvas some Canvas object
	*/
	void draw(const Canvas& canvas) override;

	/**
	 * @brief clears this arrow from canvas
	 * @param canvas given Canvas object
	*/
	void clearDraw(const Canvas& canvas) override;


	/**
	 * @brief calculates and returns the Area of this arrow
	 * @return calculated area
	*/
	virtual double getArea() const override;

	/**
	 * @brief calculates and returns the perimeter of this arrow
	 * @return calculated perimeter
	*/
	virtual double getPerimeter() const override;

	/**
	 * @brief moves this arrow using given delta point
	 * @param other - given delta point
	*/
	virtual void move(const Point& other) override;

	/**
	 * @brief returns this.points at a given index
	 * @param index - some index
	 * @return point at the specified index
	*/
	Point get_point(int index) const;

private:
	std::vector<Point> _points;
};