#pragma once
#include <map>

#include "Canvas.h"
#include "Shape.h"

class BetterCanvas 
{
public:
	// main draw functions
	void draw_shape(Shape* shape);
	void crear_shape(Shape* shape);
	
private:
	Canvas _canvas;

	// shape types
	enum type
	{
		circle,
		triangle,
		rectangle,
		arrow,
	};
	std::map<std::string, type> types{
		{"circle", circle},
		{"triangle", triangle},
		{"rectangle", rectangle},
		{"arrow", arrow},
	};

	// simple shape drawing functions
	void draw_circle(Shape* shape) const;
	void draw_triangle(Shape* shape) const;
	void draw_rectangle(Shape* shape) const;
	void draw_arrow(Shape* shape) const;

	// simple shape clearing functions
	void remove_circle(Shape* shape) const;
	void remove_triangle(Shape* shape) const;
	void remove_rectangle(Shape* shape) const;
	void remove_arrow(Shape* shape) const;
};
