#include "QuadRangle.h"

QuadRangle::QuadRangle(std::vector<Point> points, const std::string& name, const std::string& type, Color color) : Polygon(type, name, color)
{
	this->_points = points;
}

QuadRangle::~QuadRangle()
{
}

/**
 * @brief draws this quadrangle on a given Canvas object
 * @param canvas some Canvas object
*/
void QuadRangle::draw(const Canvas& canvas)
{
	canvas.draw_quadrangle(this->_points, this->_color);
}

/**
 * @brief clears this quadrangle from canvas
 * @param canvas given Canvas object
*/
void QuadRangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_quadrangle(this->_points);
}

/**
 * @brief calculates and returns the Area of this quadrangle
 * @return calculated area
*/
double QuadRangle::getArea() const
{
	return 0;
}

/**
 * @brief calculates and returns the perimeter of this quadrangle
 * @return calculated perimeter
*/
double QuadRangle::getPerimeter() const
{
	return 0;
}
