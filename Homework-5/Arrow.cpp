#include "Arrow.h"

/**
 * @brief creates new Arrow object`
 * @param a Point object representing arrow "tale"
 * @param b Point object representing arrow "head"
 * @param type - arrow type
 * @param name - arrow name
 * @param color - given color
*/
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name, const Color color) : Shape(name, type, color)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
}

Arrow::~Arrow()
{
}

/**
 * @brief draws this arrow on a given Canvas object
 * @param canvas some Canvas object
*/
void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1], this->_color);
}

/**
 * @brief clears this arrow from canvas
 * @param canvas given Canvas object
*/
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

/**
 * @brief calculates and returns the Area of this arrow
 * @return calculated area
*/
double Arrow::getArea() const
{
	// area of arrow is 0
	return 0;
}

/**
 * @brief calculates and returns the perimeter of this arrow
 * @return calculated perimeter
*/
double Arrow::getPerimeter() const
{
	// perimeter of arrow is distance between it's 2 points (head and tail)
	return this->_points[0].distance(this->_points[1]);
}

/**
 * @brief moves this arrow using given delta point
 * @param other - given delta point
*/
void Arrow::move(const Point& other)
{
	// loop through all point in vector and move each one of them
	for (Point& curr : this->_points)
	{
		curr += other;
	}
}

/**
 * @brief returns this.points at a given index
 * @param index - some index
 * @return point at the specified index
*/
Point Arrow::get_point(int index) const
{
	return this->_points[index];
}




