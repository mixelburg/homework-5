#include "Menu.h"
#include <iostream>
#include <sstream>



Menu::Menu()
{
}

Menu::~Menu()
{
}

/**
 * @brief clears the terminal **only for windows operating systems**
*/
void Menu::clear_screen()
{
	// cleans windows terminal
	system("CLS");
}

/**
 * @brief pauses the terminal **only for windows operating systems**
*/
void Menu::pause_terminal()
{
	system("PAUSE");
}


void Menu::print_main_menu()
{
	std::cout << std::endl
		<< "Main menu: " << std::endl
		<< "1. add a new shape." << std::endl
		<< "2. modify or get information from a current shape." << std::endl
		<< "3. print all shapes info" << std::endl
		<< "4. delete all of the shapes." << std::endl
		<< "0. exit." << std::endl
		<< std::endl;
}

void Menu::print_color_menu()
{
	std::cout << std::endl
		<< "Colors : " << std::endl
		<< "1. red" << std::endl
		<< "2. green" << std::endl
		<< "3. blue" << std::endl
		<< "4. white" << std::endl
		<< "5. black" << std::endl
		<< std::endl;
}

void Menu::print_create_menu()
{
	std::cout << std::endl
		<< "Create menu: " << std::endl
		<< "1. Circle" << std::endl
		<< "2. Triangle" << std::endl
		<< "3. Rectangle" << std::endl
		<< "4. Arrow" << std::endl
		<< "5. Polygon" << std::endl
		<< "0. exit." << std::endl
		<< std::endl;
}

void Menu::print_edit_menu()
{
	std::cout << std::endl
		<< "Edit menu: " << std::endl
		<< "1. Move" << std::endl
		<< "2. Change color" << std::endl
		<< "3. Print details" << std::endl
		<< "4. Delete" << std::endl
		<< "0. exit." << std::endl
		<< std::endl;
}


// helper functions

/**
 * @brief checks if string is integer number
 * @param s string
 * @return bool
*/
bool Menu::is_number(const std::string& s)
{
	// get iterator
	std::string::const_iterator it = s.begin();
	// go trough string
	while (it != s.end() && std::isdigit(*it)) ++it;

	// check if there is any non digit char
	return !s.empty() && it == s.end();
}

/**
 * @brief converts string to int
 * @param str string
 * @return int from string
*/
int Menu::to_int(const std::string& str)
{
	// get string stream
	std::stringstream sstream(str);
	// create variable for the result
	int result;
	// convert string to int
	sstream >> result;

	return result;
}

/**
 * @brief gets integer number from user
 * @param message - message to be printed to user
 * @param min_value - minimal valid value
 * @param max_value - maximal valid value
 * @return integer number from user
*/
int Menu::get_num(std::string& message, int min_value, int max_value)
{
	// variable for user input
	std::string input = get_string(message);

	// get new input until it isn't correct
	while (!(is_number(input)))
	{
		// print error message
		std::cout << MSG.at(input_type_error) << std::endl;

		// get new user input
		input = get_string(message);
	}

	// get new input untill it's valid
	if (to_int(input) < min_value or to_int(input) > max_value)
	{
		std::cout << "only numbers between " << min_value << " and " << max_value << " are allowed" << std::endl;
		return get_num(message, min_value, max_value);
	}
	return to_int(input);
}

/**
 * @brief gets string from user
 * @param message - message to be printed to user
 * @return string input from user
*/
std::string Menu::get_string(std::string& message)
{
	// create variable for input
	std::string str;
	// display given message
	std::cout << message;
	// get string from user
	std::getline(std::cin, str);

	return str;
}



/**
 * @brief prints info about all shapes
*/
void Menu::print_all_shapes()
{
	// print main info
	std::cout << "All shapes info: " << std::endl;
	std::cout << "Number of shapes : " << this->shapes.size() << std::endl << std::endl;

	// loop through all the shapes and print info about each one
	for (int i = 0; i < this->shapes.size(); ++i)
	{
		std::cout << "num: " << i << std::endl;
		this->shapes[i]->printDetails();
		std::cout << std::endl;
	}
}

/**
 * @brief Creates and adds new circle to the canvas
*/
void Menu::add_new_circle()
{
	// create center of the circle
	const Point center(get_num(MSG[enter_x]) , get_num(MSG[enter_y]));

	// get radius
	const int radius = get_num(MSG[enter_radius]);

	// get color
	const Color color = get_color();
	
	// create new circle
	Shape* new_circle = new Circle(center, radius, types[circle], get_string(MSG[enter_name]), color);
	// draw it
	new_circle->draw(this->_canvas);
	// add it to the shape list
	this->shapes.push_back(new_circle);
}

/**
 * @brief Creates and adds new triangle to the canvas
*/
void Menu::add_new_triangle()
{
	// create an array for all 3 points of a triangle
	const int num_points = 3;
	Point points[num_points];

	// initialize all of the points
	for (int i = 0; i < num_points; i++)
	{
		std::cout << "Point num: " << i + 1 << std::endl;
		points[i].set_x(get_num(MSG[enter_x]));
		points[i].set_y(get_num(MSG[enter_y]));
	}

	// get color
	const Color color = get_color();

	// create new triangle
	Shape* new_triangle = new Triangle(points[0], points[1], points[2], types[triangle], get_string(MSG[enter_name]), color);
	// draw it
	new_triangle->draw(this->_canvas);
	// add it to the shape list
	this->shapes.push_back(new_triangle);
}

/**
 * @brief Creates and adds new rectangle to the canvas
*/
void Menu::add_new_rectangle()
{
	// create top left point of the rectangle
	const Point top_left(get_num(MSG[enter_x]), get_num(MSG[enter_y]));

	const int length = get_num(MSG[enter_length]);
	const int width = get_num(MSG[enter_width]);

	// get color
	const Color color = get_color();
	
	// create new rectangle
	Shape* new_rectangle = new myShapes::Rectangle(top_left, length, width, types[rectangle], get_string(MSG[enter_name]), color);
	// draw it
	new_rectangle->draw(this->_canvas);

	// add it to the shape list
	this->shapes.push_back(new_rectangle);
}

/**
 * @brief Creates and adds new arrow to the canvas
*/
void Menu::add_new_arrow()
{
	// create an array for all 2 points of an arrow
	const int num_points = 2;
	Point points[num_points];

	// initialize all of the points
	for (int i = 0; i < num_points; i++)
	{
		std::cout << "Point num: " << i + 1 << std::endl;
		points[i].set_x(get_num(MSG[enter_x]));
		points[i].set_y(get_num(MSG[enter_y]));
	}

	// get color
	const Color color = get_color();
	
	// create new arrow
	Shape* new_arrow = new Arrow(points[0], points[1], types[arrow], get_string(MSG[enter_name]), color);
	// draw it
	new_arrow->draw(this->_canvas);
	// add it to the shape list
	this->shapes.push_back(new_arrow);
}

/**
 * @brief Creates and adds new polygon to the canvas
*/
void Menu::add_new_polygon()
{
	const int num_points = 4;
	std::vector<Point> points;
	for (int i = 0; i < num_points; ++i)
	{
		std::cout << "Point num: " << i + 1 << std::endl;
		points.push_back(Point(get_num(MSG[enter_x]), get_num(MSG[enter_y])));
	}

	// get color
	const Color color = get_color();

	Shape* polygon = new QuadRangle(points, get_string(MSG[enter_name]), types[types_l::polygon], color);
	polygon->draw(this->_canvas);
	this->shapes.push_back(polygon);
}

/**
 * @brief Moves shape at a given index
 * @param index - shape index
*/
void Menu::move_shape(const int index)
{
	this->shapes[index]->clearDraw(this->_canvas);
	// move the shape
	this->shapes[index]->move(Point(get_num(MSG[enter_x]), get_num(MSG[enter_y])));
	// draw all of the shapes one more time
	draw_all_shapes();
}

/**
 * @brief Changes color of the shape at a given index
 * @param index - shape index
*/
void Menu::change_shape_color(const int index)
{
	clear_screen();
	// set new color
	this->shapes[index]->set_color(get_color());
	// erase the shape from the canvas
	this->shapes[index]->clearDraw(this->_canvas);
	// draww the shape on the canvas
	this->shapes[index]->draw(this->_canvas);
}

/**
 * @brief Deletes the shape at a given index
 * @param index - shape index
*/
void Menu::delete_shape(int index)
{
	this->shapes[index]->clearDraw(this->_canvas);
	this->shapes.erase(this->shapes.begin() + index);
	draw_all_shapes();
}

/**
 * @brief deletes all of the shapes from canvas
*/
void Menu::delete_all_shapes()
{
	// loop through all of the shapes and clear tam from canvas
	for (Shape* element : this->shapes)
	{
		element->clearDraw(this->_canvas);
	}
	// clear the shapes list
	this->shapes.clear();
}

/**
 * @brief draws all of the shapes on the canvas
*/
void Menu::draw_all_shapes()
{
	// loop through all of the elements and draw each one
	for (Shape* element : this->shapes)
	{
		element->draw(this->_canvas);
	}
}

/**
 * @brief lets user choose the color and returns it
 * @return chosen color
*/
Color Menu::get_color()
{
	print_color_menu();
	const int choice = get_num(MSG[enter_color], 1, 5);

	// return color based on user choice
	switch (choice)
	{
	case choices::color_red:
		return Color::red;
	case choices::color_green:
		return Color::green;
	case choices::color_blue:
		return Color::blue;
	case choices::color_white:
		return Color::white;
	case choices::color_black:
		return Color::black;
	default:
		return Color::white;
	}
}

/**
 * @brief processes creation menu
*/
void Menu::creation_menu()
{
	// clear screen and print the menu
	clear_screen();
	print_create_menu();
	const int choice = get_num(MSG[enter_choice]);

	// main logic switch
	switch (choice)
	{
	case choices::add_circle_c:
		add_new_circle();
		break;
	case choices::add_triangle_c:
		add_new_triangle();
		break;
	case choices::add_rectangle_c:
		add_new_rectangle();
		break;
	case choices::add_arrow_c:
		add_new_arrow();
		break;
	case choices::add_polygon_c:
		add_new_polygon();
		break;
	default:
		break;
	}
}

/**
 * @brief processes edit menu
*/
void Menu::edit_menu()
{
	// clear screen and print the menu
	clear_screen();
	print_edit_menu();
	const int choice = get_num(MSG[enter_choice]);
	if (choice == choices::exit_c) return;

	// get index of a shape to edit
	clear_screen();
	print_all_shapes();
	const int index = get_num(MSG[enter_choice], 0, this->shapes.size() - 1);

	// main logic switch
	switch (choice)
	{
	case choices::move_shape_c:
		move_shape(index);
		clear_screen();
		break;
	case choices::change_color_c:
		change_shape_color(index);
		clear_screen();
		break;
	case choices::print_details_c:
		clear_screen();
		this->shapes[index]->printDetails();
		pause_terminal();
		clear_screen();
		break;
	case choices::delete_shape_c:
		delete_shape(index);
		clear_screen();
		break;
	default:
		break;
	}
}

/**
 * @brief main menu processor function
 * @param choice - user choice
*/
void Menu::main_menu(int choice)
{
	clear_screen();
	// perform some action based on user choice
	switch (choice)
	{
	case choices::create_new_shape_c:
		creation_menu();
		clear_screen();
		break;
	case choices::edit_shape_c:
		edit_menu();
		clear_screen();
		break;
	case choices::print_all_shapes_c:
		clear_screen();
		print_all_shapes();
		pause_terminal();
		clear_screen();
		break;
	case choices::remove_all_shapes_c:
		delete_all_shapes();
		clear_screen();
		break;
	case choices::exit_c:
		std::cout << MSG[exit_msg] << std::endl;
		break;
	default:
		std::cout << MSG.at(invalid_choice_error) << std::endl;
		break;
	}

}

void Menu::start()
{
	Point center(100, 100);
	std::string name = "name";
	std::string type = "simple circle";
	Shape* circle = new Circle(center, 50, type, name, Color::green);
	circle->draw(this->_canvas);
	
	Point center2(50, 50);
	std::string name2 = "name2";
	std::string type2 = "simple circle2";
	Shape* circle2 = new Circle(center2, 40, type2, name2, Color::blue);
	circle2->draw(this->_canvas);

	std::vector<Point> points;
	points.push_back(Point(200, 300));
	points.push_back(Point(100, 100));
	points.push_back(Point(150, 500));
	points.push_back(Point(600, 600));
	points.push_back(Point(10, 10));


	QuadRangle* poly = new QuadRangle(points, name, type, Color::red);
	poly->draw(this->_canvas);
	
	this->shapes.push_back(circle);
	this->shapes.push_back(circle2);
	this->shapes.push_back(poly);

	// main program loop
	int choice;
	do
	{
		print_main_menu();
		choice = get_num(MSG.at(enter_choice));
		main_menu(choice);
	} while (choice != choices::exit_c);
}


