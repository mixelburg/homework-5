#include "Polygon.h"

/**
 * @brief creates new Polygon object
 * @param type - type of the polygon
 * @param name - name of the polygon
 * @param color - given color
*/
Polygon::Polygon(const std::string& type, const std::string& name, const Color color) : Shape(name, type, color)
{
}

/**
 * @brief moves this polygon using given delta point
 * @param other - given delta point
*/
void Polygon::move(const Point& other)
{
	// loop through all point in vector and move each one of them
	for (Point& curr : this->_points)
	{
		curr += other;
	}
}

/**
 * @brief returns this.points at a given index
 * @param index - some index
 * @return point at the specified index
*/
Point Polygon::get_point(const int index) const
{
	return this->_points[index];
}

