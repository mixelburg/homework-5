#include "Rectangle.h"

/**
 * @brief creates new Rectangle object
 * @param a - Point object representing top left corner of the rectangle
 * @param length - length of the rectangle
 * @param width - width of the rectangle
 * @param type - type of the rectangle
 * @param name - name of the rectangle
 * @param color - given color
*/
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type,
	const std::string& name, const Color color) : Polygon(type, name, color), _lenght(length), _width(width)
{
	this->_points.push_back(a);
	Point second(a);
	Point delta(length, width);
	second += delta;
	this->_points.push_back(second);
}

myShapes::Rectangle::~Rectangle()
{
}

/**
 * @brief draws this rectangle on a given Canvas object
 * @param canvas some Canvas object
*/
void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1], this->_color);
}

/**
 * @brief clears this rectangle from canvas
 * @param canvas given Canvas object
*/
void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

/**
 * @brief calculates and returns the Area of this rectangle
 * @return calculated area
*/
double myShapes::Rectangle::getArea() const
{
	// calculate and return area of this rectangle using simple formula
	return this->_width * this->_lenght;
}

/**
 * @brief calculates and returns the perimeter of this rectangle
 * @return calculated perimeter
*/
double myShapes::Rectangle::getPerimeter() const 
{
	// calculate and return perimeter of this rectangle using simple formula
	return (this->_width + this->_lenght) * 2;
}
