#include "Triangle.h"
#include <cmath> 

/**
 * @brief creates new Triangle object
 * @param a Point object representing first point
 * @param b Point object representing second point
 * @param c Point object representing third point
 * @param type - triangle type
 * @param name - triangle name
 * @param color - given color
*/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name, const Color color) : Polygon(type, name, color)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

Triangle::~Triangle()
{
}

/**
 * @brief draws this triangle on a given Canvas object
 * @param canvas some Canvas object
*/
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2], this->_color);
}

/**
 * @brief clears this triangle from canvas
 * @param canvas given Canvas object
*/
void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

/**
 * @brief calculates and returns the Area of this triangle
 * @return calculated area
*/
double Triangle::getArea() const
{
	// calculate and return triangle area using formula:
	// S = |( A.x * (B.y - C.y) + B.x * (C.y - A.y) + C.x(A.y - B.y) ) / 2|
	// |x| stands for absolute value of x
	
	return 	std::abs((this->_points[0].getX() * (this->_points[1].getY() - this->_points[2].getY()) +
						 this->_points[1].getX() * (this->_points[2].getY() - this->_points[0].getY()) +
					     this->_points[2].getX() * (this->_points[0].getY() - this->_points[1].getY())) / 2);
}

/**
 * @brief calculates and returns the perimeter of this triangle
 * @return calculated perimeter
*/
double Triangle::getPerimeter() const
{
	// calculate and perimeter by calculating lengths of all sides and simply returning their sum
	return this->_points[0].distance(this->_points[1]) +
		this->_points[1].distance(this->_points[2]) +
		this->_points[2].distance(this->_points[0]);
}
