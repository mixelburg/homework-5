#pragma once
#include <map>
#include <vector>

#include "Shape.h"
#include "Canvas.h"
#include "BetterCanvas.h"
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Arrow.h"
#include "QuadRangle.h"

class Menu
{
public:

	Menu();
	~Menu();

	void start();

	/**
	 * @brief clears the terminal **only for windows operating systems**
	*/
	static void clear_screen();

	/**
	 * @brief pauses the terminal **only for windows operating systems**
	*/
	static void pause_terminal();

	/**
	 * @brief checks if string is integer number
	 * @param s string
	 * @return bool
	*/
	bool is_number(const std::string& s);

	/**
	 * @brief converts string to int
	 * @param str string
	 * @return int from string
	*/
	int to_int(const std::string& str);

	/**
	 * @brief gets integer number from user
	 * @param message - message to be printed to user
	 * @param min_value - minimal valid value
	 * @param max_value - maximal valid value
	 * @return integer number from user
	*/
	int get_num(std::string& message, int min_value = INT_MIN, int max_value = INT_MAX);

	/**
	 * @brief gets string from user
	 * @param message - message to be printed to user
	 * @return string input from user
	*/
	static  std::string get_string(std::string& message);

	/**
	 * @brief prints info about all shapes
	*/
	void print_all_shapes();

	// new figure creators
	void add_new_circle();
	void add_new_triangle();
	void add_new_rectangle();
	void add_new_arrow();
	void add_new_polygon();

	// shape editors

	/**
	 * @brief Moves shape at a given index
	 * @param index - shape index
	*/
	void move_shape(int index);

	/**
	 * @brief Changes color of the shape at a given index
	 * @param index - shape index
	*/
	void change_shape_color(int index);

	/**
	 * @brief Deletes the shape at a given index
	 * @param index - shape index
	*/
	void delete_shape(int index);
	
	/**
	 * @brief deletes all of the shapes from canvas
	*/
	void delete_all_shapes();

	/**
	 * @brief draws all of the shapes on the canvas
	*/
	void draw_all_shapes();
	
	/**
	 * @brief lets user choose the color and returns it
	 * @return chosen color
	*/
	Color get_color();

	// simple menu printers
	void print_main_menu();
	void print_color_menu();
	void print_create_menu();
	void print_edit_menu();
	
	// submenu processors
	void creation_menu();
	void edit_menu();
	
	/**
	 * @brief main menu processor function
	 * @param choice - user choice
	*/
	void main_menu(int choice);

private: 
	Canvas _canvas;
	std::vector<Shape*> shapes;
	
	enum choices
	{
		exit_c = 0,

		create_new_shape_c = 1,
		edit_shape_c = 2,
		print_all_shapes_c = 3,
		remove_all_shapes_c = 4,

		add_circle_c = 1,
		add_triangle_c = 2,
		add_rectangle_c = 3,
		add_arrow_c = 4,
		add_polygon_c = 5,

		move_shape_c = 1,
		change_color_c = 2,
		print_details_c = 3,
		delete_shape_c = 4,

		color_red = 1,
		color_green = 2,
		color_blue = 3,
		color_white = 4,
		color_black = 5,
	};

	enum types_l
	{
		circle,
		triangle,
		rectangle,
		arrow,
		polygon,
	};
	std::map<types_l, std::string> types = {
		{circle, "circle"},
		{triangle, "triangle"},
		{rectangle, "rectangle"},
		{arrow, "arrow"},
		{polygon, "polygon"},
	};
	

	// text id's
	enum msgs
	{
		// info messages
		enter_choice,
		enter_x,
		enter_y,
		enter_radius,
		enter_name,
		enter_width,
		enter_length,
		enter_color,
		enter_num_points,
		
		// exit message
		exit_msg,

		// error messages
		invalid_choice_error,
		input_type_error,
	};
	// map with texts
	std::map<msgs, std::string> MSG = {
		// info messages
		{enter_choice, "enter choice: "},
		{enter_x, "enter x: "},
		{enter_y, "enter y: "},
		{enter_radius, "enter radius: "},
		{enter_name, "enter name: "},
		{enter_width, "enter width: "},
		{enter_length, "enter length: "},
		{enter_color, "enter color: "},
		{enter_num_points, "enter number of points: "},
		
		// exit message
		{exit_msg, "[+] exiting..."},

		// error messages
		{invalid_choice_error, "[!] invalid choice"},
		{input_type_error, "[!] only numbers allowed, try again"},
	};
};

