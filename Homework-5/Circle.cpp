#include "Circle.h"

#include <cmath>

/**
 * @brief creates new Circle object
 * @param center - point representing center of the circle
 * @param radius - radius of the circle
 * @param type - type of the circle
 * @param name - name of the circle
 * @param color - color of the circle
*/
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name, Color color) :
Shape(name, type, color), _center(center), _radius(radius)
{
}

Circle::~Circle()
{
}

// simple getters
const Point& Circle::get_center() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

/**
 * @brief draws this circle on given Canvas object
 * @param canvas - given Canvas object
*/
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(get_center(), getRadius(), this->_color);
}

/**
 * @brief removes this circle from given Canvas object
 * @param canvas - given Canvas object
*/
void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(get_center(), getRadius());
}

/**
 * @brief calculates area of this circle and returns it
 * @return calculated area
*/
double Circle::getArea() const
{
	// calculate and return perimeter by using simple formula
	return PI * pow(this->_radius, 2);
}

/**
 * @brief calculates perimeter of this circle and returns it
 * @return calculated perimeter
*/
double Circle::getPerimeter() const
{
	// calculate and return radius by using simple formula
	return 2 * PI * this->_radius;
}

/**
 * @brief moves this circle based on the given delta Point object
 * @param other given delta Point object
*/
void Circle::move(const Point& other)
{
	// just add this.center Point object to given delta Point object
	this->_center += other;
}


