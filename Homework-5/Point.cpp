#include "Point.h"

#include <cmath>

/**
 * @brief creates new Point object
 * @param x - x coordinate of the point
 * @param y - y coordinate of the point
*/
Point::Point(double x, double y) : _x(x), _y(y)
{
}

Point::Point() : _x(-9999), _y(-9999)
{
}

Point::Point(const Point& other) : _x(other._x), _y(other._y)
{
}

Point::~Point()
{
}

/**
 * @brief adds this.x to other.x and this.y to other.y
 * @param other - other Point object
*/
void Point::add_x_y(Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
}

/**
 * @brief returns new Point object that is sum of this and other points
 * basically adds this.x to other.x and this.y to other.y
 * @param other - other given Point object
 * @return new Point object that is sum of this and other
*/
Point Point::operator+(const Point& other) const
{
	Point result(*this);

	result._x += other._x;
	result._y += other._y;

	return result;
}

/**
 * @brief adds other point to this point and returns pointer to this point
 * basically adds this.x to other.x and this.y to other.y
 * @param other - other given Point object
 * @return pointer to this point
*/
Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	
	return *this;
}

// simple getters
double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

void Point::set_x(const double x)
{
	_x = x;
}

void Point::set_y(const double y)
{
	_y = y;
}

/**
 * @brief calculates distance from this point to a given point and returns it
 * @param other given point
 * @return calculated distance between points
*/
double Point::distance(const Point& other) const
{
	// calculate distance using basic formula
	return sqrt(pow((this->_x - other._x), 2) + pow((this->_y - other._y), 2));
}
