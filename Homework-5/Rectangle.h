#pragma once
#include "Polygon.h"
#include "Point.h"

namespace myShapes
{
	// Calling it MyRectangle because Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		// There's a need only for the top left corner 

		/**
		 * @brief creates new Rectangle object
		 * @param a - Point object representing top left corner of the rectangle
		 * @param length - length of the rectangle
		 * @param width - width of the rectangle
		 * @param type - type of the rectangle
		 * @param name - name of the rectangle
		 * @param color - given color
		*/
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name, Color color);
		virtual ~Rectangle() override;

		/**
		 * @brief draws this rectangle on a given Canvas object
		 * @param canvas some Canvas object
		*/
		void draw(const Canvas& canvas) override;

		/**
		 * @brief clears this rectangle from canvas
		 * @param canvas given Canvas object
		*/
		void clearDraw(const Canvas& canvas) override;

		/**
		 * @brief calculates and returns the Area of this rectangle
		 * @return calculated area
		*/
		virtual double getArea() const override;

		/**
		 * @brief calculates and returns the perimeter of this rectangle
		 * @return calculated perimeter
		*/
		virtual double getPerimeter() const override;

	private:
		double _lenght;
		double _width;
	};
}