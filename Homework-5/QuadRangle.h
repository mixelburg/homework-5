#pragma once
#include "Polygon.h"

class QuadRangle : public Polygon
{
public:
	QuadRangle(std::vector<Point> points, const std::string& name, const std::string& type, Color color);
	~QuadRangle() override;

	/**
	 * @brief draws this quadrangle on a given Canvas object
	 * @param canvas some Canvas object
	*/
	void draw(const Canvas& canvas) override;

	/**
	 * @brief clears this quadrangle from canvas
	 * @param canvas given Canvas object
	*/
	void clearDraw(const Canvas& canvas) override;

	/**
	 * @brief calculates and returns the Area of this quadrangle
	 * @return calculated area
	*/
	virtual double getArea() const override;

	/**
	 * @brief calculates and returns the perimeter of this quadrangle
	 * @return calculated perimeter
	*/
	virtual double getPerimeter() const override;
};
