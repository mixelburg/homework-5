#define _CRT_SECURE_NO_WARNINGS

#include "Bonus.h"

#include <iostream>
#include <vector>


File::File(const std::string& name, const char* mode)
{
	this->file_fd = fopen(name.c_str(), mode);
	// print error if file name is invalid
	if (this->file_fd == NULL) {
		std::cerr << "Unable to open file" << std::endl;
		exit(1);
	}
}

File::~File()
{
	fclose(this->file_fd);
}

File::operator _iobuf*&()
{
	return this->file_fd;
}


void grep::find_string(const std::string& file_name, const std::string& to_find)
{
	std::vector<std::string> instances;

	// open file for reading
	File new_file_object(file_name, "r");
	FILE* fp = new_file_object;

	// convert const char* to char*
	char* cstr = new char[to_find.length() + 1];
	strcpy(cstr, to_find.c_str());

	// read file line by line
	char chunk[line_size];
	while (fgets(chunk, sizeof(chunk), fp) != NULL) {
		if (check_for_substring(cstr, chunk))
		{
			instances.push_back(std::string(chunk));
		}
	}

	// print search info
	std::cout << "Searching for string : " << to_find << std::endl;
	std::cout << "Found " << instances.size() << " instances in file : " << file_name << std::endl;
	for (const std::string& element : instances)
	{
		std::cout << "\t -> " << element << std::endl;
	}
	
	delete[] cstr;
}

bool grep::check_for_substring(const char* substring, const  char* sent)
{
	const char* result = strstr(sent, substring);
	if (result)
	{
		return true;
	}
	return false;
}
