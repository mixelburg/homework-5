#pragma once

#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	/**
	 * @brief creates new Triangle object 
	 * @param a Point object representing first point
	 * @param b Point object representing second point
	 * @param c Point object representing third point
	 * @param type - triangle type
	 * @param name - triangle name
	 * @param color - given color
	*/
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name, Color color);
	virtual ~Triangle() override;
	
	/**
	 * @brief draws this triangle on a given Canvas object
	 * @param canvas some Canvas object
	*/
	void draw(const Canvas& canvas) override;
	
	/**
	 * @brief clears this triangle from canvas
	 * @param canvas given Canvas object
	*/
	void clearDraw(const Canvas& canvas) override;

	/**
	 * @brief calculates and returns the Area of this triangle
	 * @return calculated area
	*/
	virtual double getArea() const override;

	/**
	 * @brief calculates and returns the perimeter of this triangle
	 * @return calculated perimeter
	*/
	virtual double getPerimeter() const override;
};