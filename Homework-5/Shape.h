#pragma once
#include "Point.h"
#include "Canvas.h"
#include <string>

/**
 * @brief class representing a basic shape 
*/
class Shape 
{
public:
	/**
	 * @brief builds new Shape object
	 * @param name - given name
	 * @param type - given type
	 * @param color - given color
	*/
	Shape(const std::string& name, const std::string& type, Color color);
	virtual ~Shape();

	/**
	 * @brief has to calculate and return the Area of this shape
	 * @return calculated area
	*/
	virtual double getArea() const = 0;

	/**
	 * @brief has to calculate and return the perimeter of this shape
	 * @return calculated perimeter
	*/
	virtual double getPerimeter() const = 0;

	/**
	 * @brief has to draw this shape on a given Canvas object
	 * @param canvas some Canvas object
	*/
	virtual void draw(const Canvas& canvas) = 0;

	/**
	 * @brief has to clear this shape from canvas
	 * @param canvas given Canvas object
	*/
	virtual void clearDraw(const Canvas& canvas) = 0;

	/**
	 * @brief has to move this shape according to given delta point
	 * @param other delta point
	*/
	virtual void move(const Point& other) = 0; // add the Point to all the points of shape
	
	/**
	 * @brief prints details about this shape
	*/
	void printDetails() const;

	/**
	 * @brief sets this.color to a given color
	 * @param color - given color
	*/
	void set_color(Color color);

	/**
	 * @brief returns color of the shape
	 * @return Color object
	*/
	Color get_color() const;
	

	// simple getters
	std::string getType() const;
	std::string getName() const;

private:
	std::string _name;
	std::string _type;

protected:
	Color _color;
};